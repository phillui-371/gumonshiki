/**
 * Need add Document beginning codeblocks handling for metadata including
 * 1. title
 * 2. date (optional)
 * 3. tags (optional, list of striped string)
 * 4. category
 * 5. draft (optional, if appear won't be handled)
 */

/**
 * Some syntax change compare with Markdown standard
 * 1. "=" or "-" below text to change text size is not supported
 * 2. bold/italic can use asterisk "*"" only, underscore "_" is not supported
 * 3. Code block only support Fenced one, i.e. Cannot be inserted into list
 * 4. Table only support syntax which start and end with pipe
 * 5. Fenced Code Blocks support ``` only, ~~~ will not supported
 * 6. Emoji shortcode will not support
 */

// https://www.markdownguide.org/basic-syntax/
// https://www.markdownguide.org/extended-syntax/
// https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

// Basic part
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TextSize {
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TextStyle {
    Bold,
    Italic,
    BoldItalic,
    StrikeThrough
}

/**
 * eg. ## abc**d**
 * should be vec![
 *   Text(content="abc",size=Some(TextSize::H2),style=None),
 *   Text(content="d",size=Some(TextSize::H2),style=Some(TextStyle::Bold))
 * ]
 **/
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Text {
    content: String,
    size: Option<TextSize>,
    style: Option<TextStyle>,
    with_linebreaks: bool,
    id: Option<String>
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Paragraph {
    texts: Vec<Text>
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Blockquotes {
    texts: Vec<Text>,
    children: Vec<Box<Blockquotes>> // https://www.markdownguide.org/basic-syntax/#nested-blockquotes
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct CodeBlocks {
    texts: Vec<String>,
    lang: Option<String>
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum LinkDisplay<'a> {
    Text(String),
    Image(&'a Image)
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Link<'a> {
    display: LinkDisplay<'a>,
    url: String,
    title: Option<String>,
    style: Option<TextStyle>,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct RefLink<'a> {
    ref_link: &'a Link<'a>
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Image {
    url: String,
    title: Option<String>,
    alt: Option<String>,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct HoriRules {}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ListItem {
    Texts(Vec<Text>),
    Blockquotes(Vec<Text>, Blockquotes)
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct List {
    items: Vec<ListItem>,
    children: Vec<Box<List>>
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct OrderedList {
    content: List
}

// Extended part
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Alignment {
    Right,
    Middle,
    Left
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TableCellFormat {
    Link,
    Code,
    Bold,
    Italic,
    BoldItalic,
    StrikeThrough
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct TableCell {
    alignment: Alignment,
    content: String,
}

type TableRow = Vec<TableCell>;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Table {
    header: TableRow,
    rows: Vec<TableRow>
}

// Footnote should be in order in same page
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Footnote {
    tag: String,
    content: String
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct DefinitionList {
    term: String,
    content: List
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct TaskList {
    done_ls: Vec<bool>,
    content: List
}